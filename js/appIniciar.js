import { signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-auth.js"
import { auth } from "./app.js";

const InicioForm = document.querySelector("#IniciarSesion");

InicioForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  const email = InicioForm["correoIniciar"].value;
  const password = InicioForm["contraseñaIniciar"].value;

  try {
    const user = await signInWithEmailAndPassword(auth, email, password)
    console.log(user)
    window.location.href = "/html/administrador.html";

  } catch (error) {
    if (error.code === 'auth/wrong-password') {
      alert("Wrong password")
    } else if (error.code === 'auth/user-not-found') {
      alert("User not found")
    } else {
      alert("Something went wrong")
    }
  }
});