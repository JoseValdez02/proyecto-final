// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import {getAuth} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js"
import {getDatabase, ref, set, child, update, remove} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js"
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD8GdwgvjnExB4H515rGds0KY4sQo9F818",
  authDomain: "sitioweb-a4c3b.firebaseapp.com",
  databaseURL: "https://sitioweb-a4c3b-default-rtdb.firebaseio.com",
  projectId: "sitioweb-a4c3b",
  storageBucket: "sitioweb-a4c3b.appspot.com",
  messagingSenderId: "67341558955",
  appId: "1:67341558955:web:a288e35d94c6ef7f7e43ee"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)
export const db = getDatabase(app);
export const storage = getStorage(app);
