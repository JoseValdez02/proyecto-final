import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';
import { storage } from './app.js';  // Asegúrate de importar la instancia de storage desde tu app.js

// Obtén referencias a los elementos del DOM
const imagenInput = document.getElementById('Imagen');
const subirImagenBtn = document.getElementById('Subirimagen');

// Agrega un evento de clic al botón para manejar la subida de imagen
subirImagenBtn.addEventListener('click', () => {
  // Obtener el archivo de imagen seleccionado por el usuario
  const imagenFile = imagenInput.files[0];

  // Verificar si se seleccionó un archivo
  if (!imagenFile) {
    alert("Por favor, selecciona una imagen.");
    return;
  }

  // Crear una referencia única para el archivo de imagen en Storage
  const imagenRef = storageRef(storage, 'imagenes_productos/' + imagenFile.name);

  // Subir el archivo de imagen a Storage
  const uploadTask = uploadBytesResumable(imagenRef, imagenFile);

  uploadTask
    .then((snapshot) => getDownloadURL(snapshot.ref)) // Obtener la URL de descarga después de que la carga se completa
    .then((url) => {
      // Puedes hacer algo con la URL de descarga si es necesario
      console.log("URL de descarga:", url);
      alert("Imagen subida correctamente");
    })
    .catch((error) => {
      console.error("Error al subir la imagen:", error);
      alert("No se pudo subir la imagen" + error);
    });
});
