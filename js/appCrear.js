import {createUserWithEmailAndPassword} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js"
import { auth } from './app.js'

const RegistroForm = document.querySelector("#Registro");
RegistroForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  
  const email = document.querySelector("#correoCrear").value;
  const password = document.querySelector("#contraseñaCrear").value;

console.log(email,password)

  try {
    const user = await createUserWithEmailAndPassword(auth, email, password);
    console.log(user);
    window.location.href = "/html/menu.html";
    
} catch (error) {
   console.log(error.message)
   console.log(error.code)

   if (error.code === 'auth/invalid-email'){
    alert('Invalid e-mail')
   } else if (error.code === 'auth/weak-password') {
    alert("Weak password")
  } else if (error.code) {
    alert("Something went wrong")
  }
  }
});