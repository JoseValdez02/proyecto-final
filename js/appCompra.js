import {ref, set} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js"
import { db } from "./app.js";

var ciudad = document.getElementById("Ciudad");
var estado = document.getElementById("Estado");
var calle = document.getElementById("Calle");
var numcasa = document.getElementById("NumeroCasa");
var colonia = document.getElementById("Colonia");
var referencia = document.getElementById("Referencia");
var numtarjeta = document.getElementById("Tarjeta");
var fecha = document.getElementById("Fecha");
var cvv = document.getElementById("CVV");

var Comprarbtn = document.getElementById("Comprar");

function InsertData(){
  set(ref(db, "Compras/"+ cvv.value),{
    Ciudad: ciudad.value,
    Estado: estado.value,
    Calle: calle.value,
    NumeroCasa: numcasa.value,
    Colonia: colonia.value,
    Referencia: referencia.value,
    Tarjeta: numtarjeta.value,
    Fecha: fecha.value,
    CVV: cvv.value
  })
  .then(()=>{
    alert("Se realizo la compra con exito");
    window.location.href = "/html/compraRealizada.html"
  })
  .catch((error)=>{
    alert("unsucceful, error"+error);
  });
}

Comprarbtn.addEventListener('click',InsertData);
